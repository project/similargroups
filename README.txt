OG Similar Groups
=================


Installation & Configuration
----------------------------

You need to install the Recommender API (7.x-6.x) module and Drupal Computing module before install this module.

After installation, follow these steps to compute recommendations:

  1. Run Drupal Cron to feed OG group membership data into recommender.
  2. Go to admin/config/system/computing/list, and click "OG Similar Groups Recommender" to add a computing command.
  3. Compute recommendations using either of the following approaches:
    - Open a command line terminal and run "drush recommender-run".
    - Open a command line terminal and execute the Recommender Java agent.
    - Go to admin/config/system/computing/recommender and click "Run Recommender" (not recommended).
  . You can view the execution results at admin/config/system/computing/records.
  4. Display recommendation results using the default Views.

For more information about how the module works, please read the documentation of Recommender API.


Customization
-------------

The design philosophy of this module is to "keep it simple". You need customization on the code level to have these features:

  1. Support "group" entity types other than "node".
  2. Provide "Users who posted in this group also posted in ...".
  3. Use algorithms other than item-based collaborative filtering.

Read code to see how to customize.